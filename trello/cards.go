package trello

import (
	"encoding/json"
	"net/url"
	"strconv"
	"strings"
)

type (
	/*
		{
	    "id": "576abca23b733adf9cd0c82f",
	    "badges": {
	        "votes": 0,
	        "viewingMemberVoted": false,
	        "subscribed": false,
	        "fogbugz": "",
	        "checkItems": 5,
	        "checkItemsChecked": 1,
	        "comments": 4,
	        "attachments": 1,
	        "description": true,
	        "due": "2016-06-26T03:00:00.000Z"
	    },
	    "checkItemStates": [
	        {
	            "idCheckItem": "576d5bb36bc6081ef7fa31f5",
	            "state": "complete"
	        }
	    ],
	    "closed": false,
	    "dateLastActivity": "2016-06-25T17:21:29.301Z",
	    "desc": "Now that we know we can migrate on the latest ectophoenix, consult the ERD diagram. Choose the top priority tables and generate migrations. \n\nWill actually run them from another trellycard.\n\n\nERD: https://drive.google.com/a/flowfaction.com/file/d/0B6kZvu3h2zq2MUZGZ0xqYmZTdEk/view",
	    "descData": {
	        "emoji": {}
	    },
	    "due": "2016-06-26T03:00:00.000Z",
	    "email": null,
	    "idBoard": "576aba90a0eb8065092fe7d4",
	    "idChecklists": [
	        "576d5ba53572560dc56506af"
	    ],
	    "idLabels": [
	        "576aba9084e677fd363b73ce",
	        "576d5b4f84e677fd3643ae6f"
	    ],
	    "idList": "576abb1ee6fa7ddcb3d16dd8",
	    "idMembers": [],
	    "idShort": 5,
	    "idAttachmentCover": null,
	    "manualCoverAttachment": false,
	    "labels": [
	        {
	            "id": "576aba9084e677fd363b73ce",
	            "idBoard": "576aba90a0eb8065092fe7d4",
	            "name": "live",
	            "color": "green",
	            "uses": 8
	        },
	        {
	            "id": "576d5b4f84e677fd3643ae6f",
	            "idBoard": "576aba90a0eb8065092fe7d4",
	            "name": "meta",
	            "color": null,
	            "uses": 2
	        }
	    ],
	    "name": "Assembly Flow: Write Migrations for top data",
	    "pos": 122879,
	    "shortUrl": "https://trello.com/c/7o8zoLVU",
	    "url": "https://trello.com/c/7o8zoLVU/5-assembly-flow-write-migrations-for-top-data"
	}
	*/

	Card struct {
		client  *Client
		Id      string `json:"id"`
		Name    string `json:"name"`
		Email   string `json:"email"`
		IdShort int    `json:"idShort"`

		IdBoard          string   `json:"idBoard"`
		IdList           string   `json:"idList"`
		IdMembers        []string `json:"idMembers"`
		Closed           bool     `json:"closed"`
		Pos              float32  `json:"pos"`
		ShortLink        string   `json:"shortLink"`
		DateLastActivity string   `json:"dateLastActivity"` //time.Time?
		Subscribed       bool     `json:"subscribed"`
		ShortUrl         string   `json:"shortUrl"`
		Url              string   `json:"url"`
		Due              string   `json:"due"`
		Desc             string   `json:"desc"`
	}
)

func NewCard(name, desc, due string, pos float32) Card {
	return Card{
		Name: name,
		Desc: desc,
		Pos:  pos,
		Due:  due,
	}
}

func (c *Client) GetCard(cardId string) (card *Card, err error) {
	body, err := c.Get("/card/" + cardId)
	if err != nil {
		return
	}

	err = json.Unmarshal(body, &card)
	card.client = c
	return
}

func (c *Client) AddCard(listId string, data Card) (*Card, error) {
	data.IdList = listId

	params := url.Values{}
	params.Set("name", data.Name)
	params.Set("desc", data.Desc)
	params.Set("pos", strconv.Itoa(int(data.Pos)))
	params.Set("due", data.Due)
	params.Set("idList", data.IdList)
	params.Set("idMembers", strings.Join(data.IdMembers, ","))

	body, err := c.Post("/cards", params)
	if err != nil {
		return nil, err
	}

	var card Card
	if err = json.Unmarshal(body, &card); err != nil {
		return nil, err
	}
	card.client = c
	return &card, nil
}

func (c *Client) DeleteCard(cardId string) (err error) {
	_, err = c.Delete("/card/" + cardId)
	return
}
