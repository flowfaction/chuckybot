package trello

import (
	"encoding/json"
	"net/url"
	"strconv"
	"strings"
)

type (
	List struct {
		client  *Client
		Id      string  `json:"id"`
		Name    string  `json:"name"`
		Closed  bool    `json:"closed"`
		IdBoard string  `json:"idBoard"`
		Pos     float32 `json:"pos"`
	}
)

func (c *Client) GetList(listId string) (list *List, err error) {
	body, err := c.Get("/lists/" + listId)
	if err != nil {
		return
	}

	err = json.Unmarshal(body, &list)
	list.client = c
	return
}

// AddCard creates with the attributes of the supplied Card struct
// https://developers.trello.com/advanced-reference/card#post-1-cards
func (l *List) AddCard(data Card) (*Card, error) {
	data.IdList = l.Id

	params := url.Values{}
	params.Set("name", data.Name)
	params.Set("desc", data.Desc)
	params.Set("pos", strconv.Itoa(int(data.Pos)))
	params.Set("due", data.Due)
	params.Set("idList", data.IdList)
	params.Set("idMembers", strings.Join(data.IdMembers, ","))

	body, err := l.client.Post("/cards", params)
	if err != nil {
		return nil, err
	}

	var card Card
	if err = json.Unmarshal(body, &card); err != nil {
		return nil, err
	}
	card.client = l.client
	return &card, nil
}
