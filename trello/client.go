package trello

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

type (
	Client struct {
		http     *http.Client
		endpoint string
		version  string
	}

	bearerRoundTripper struct {
		RoundTripper http.RoundTripper //TODO: embed?
		key          string
		token        string //*string??
	}
)

const (
	VERSION   = "1"
	END_POINT = "https://api.trello.com/" + VERSION
	API_KEY   = "11ab831751abd70ef97ce1814c710fb3"
	TOKEN     = "16b0243272d29b13352f20aa7bd5f8d5aa8f3250cb88f4241881ba7182c48adb"
)

func NewClient(appKey, token string) *Client {
	client := &http.Client{
		Transport: NewBearerTokenTransport(appKey, token),
	}
	return &Client{
		http:     client,
		endpoint: END_POINT,
		version:  VERSION,
	}
}

func (c *Client) do(req *http.Request) ([]byte, error) {
	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if body, err := ioutil.ReadAll(resp.Body); err == nil {
		if resp.StatusCode != 200 {
			err = fmt.Errorf(`Received status code %d making request to server with data "%s"`, resp.StatusCode, string(body))
			return nil, err
		}
		return body, nil
	} else {
		return nil, err
	}
}

func (c *Client) Get(resource string) ([]byte, error) {
	req, err := http.NewRequest("GET", c.endpoint+resource, nil)
	if err != nil {
		return nil, err
	}
	return c.do(req)
}

func (c *Client) Post(resource string, data url.Values) ([]byte, error) {
	req, err := http.NewRequest("POST", c.endpoint+resource, strings.NewReader(data.Encode()))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	return c.do(req)
}

func (c *Client) Delete(resource string) ([]byte, error) {
	req, err := http.NewRequest("DELETE", c.endpoint+resource, nil)
	if err != nil {
		return nil, err
	}

	return c.do(req)
}

//NewBearerTokenTransport creates an http.RoundTripper that adds the API key and token to API requests
func NewBearerTokenTransport(apiKey, token string) *bearerRoundTripper {
	return &bearerRoundTripper{
		key:   apiKey,
		token: token,
	}
}

//RoundTrip is a RoundTripper interface implementation that supports bearer token requests/transport
func (b *bearerRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	if b.RoundTripper == nil {
		b.RoundTripper = http.DefaultTransport
	}
	params := req.URL.Query()
	params.Add("key", b.key)
	params.Add("token", b.token)
	req.URL.RawQuery = params.Encode()

	return b.RoundTripper.RoundTrip(req)
}
