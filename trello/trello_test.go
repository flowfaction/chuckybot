package trello

import (
	"fmt"
	"testing"
)

var trello *Client

func init() {
	trello = NewClient(API_KEY, TOKEN)
}

func TestGetList(t *testing.T) {
	if list, err := trello.GetList("5761ca53517dee28b7b4234d"); err == nil {
		fmt.Printf("LIST: %v", list)
	} else {
		t.Error(err)
	}
}

func TestGetCard(t *testing.T) {
	if list, err := trello.GetCard("CIIqdUkA"); err == nil {
		fmt.Printf("CARD: %v", list)
	} else {
		t.Error(err)
	}
}

func TestAddCard(t *testing.T) {
	if list, err := trello.GetList("5761ca53517dee28b7b4234d"); err == nil {
		c := NewCard("test", "this is a test card", "", 1)
		if card, err := list.AddCard(c); err == nil {
			e := trello.DeleteCard(card.Id)
			if e != nil {
				t.Error(e)
			}
		} else {
			t.Error(err)
		}
	} else {
		t.Error(err)
	}
}
