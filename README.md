#Chucky D'Bot

Chuck is a very helpful bot, but requires patience.  Don't be disgusted, he may touch himself.

##Building
`go build`

##Prerequisites
Chucky's API token

##Usage
`./chuckybot {api-token}`

##Conversation Commands
* **add idea: {idea}** - adds an idea to the Ideas Trello board
	*  	Example: @chucky can you add idea: best idea ever