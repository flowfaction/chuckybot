package main

import (
	"encoding/json"
	"fmt"
	"golang.org/x/net/websocket"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"sync/atomic"
	"time"
)

type (
	Message struct {
		Id      uint64 `json:"id"`
		Type    string `json:"type"`
		Channel string `json:"channel"`
		User    string `json:"user"`
		Text    string `json:"text"`
	}

	responseRtmStart struct {
		Ok    bool         `json:"ok"`
		Error string       `json:"error"`
		Url   string       `json:"url"`
		Self  responseSelf `json:"self"`
	}

	responseSelf struct {
		Id   string `json:"id"`
		Name string `json:"name"`
	}
)

var messageId uint64

// startSlack calls rtm.start, and returns a websocket URL and user ID. The
// websocket URL can then be used to initiate an RTM session.
// https://api.slack.com/methods/rtm.start
func startSlack(token string) (wsUrl, id string, err error) {
	url := fmt.Sprintf("https://slack.com/api/rtm.start?token=%s", token)
	resp, err := http.Get(url)
	if err != nil {
		return
	}
	if resp.StatusCode != 200 {
		err = fmt.Errorf("RTM.Start API request failed with code %d", resp.StatusCode)
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return
	}

	var respObj responseRtmStart
	err = json.Unmarshal(body, &respObj)
	if err != nil {
		return
	}

	if !respObj.Ok {
		err = fmt.Errorf("Slack ERROR: %s", respObj.Error)
		return
	}

	wsUrl = respObj.Url
	id = respObj.Self.Id

	return
}

//connectSlack starts up slack's Real Time Messaging API (rtm.start) and obtains a Websocket connection.
func connectSlack(token string) (ws *websocket.Conn, id string) {
	if wsUrl, id, startErr := startSlack(token); startErr == nil {
		//maybe use gorilla ws?
		if ws, err := websocket.Dial(wsUrl, "" /*protocol*/, "https://api.slack.com/"); err == nil {
			return ws, id
		} else {
			log.Fatalf("Websocket dial failed: %s", err)
		}
	} else {
		log.Fatalf("Failed to start slack: %s", startErr)
	}
	return
}

//sendMessage sends a Slack message through the given Websocket connection
func sendMessage(ws *websocket.Conn, m Message) error {
	m.Id = atomic.AddUint64(&messageId, 1)
	//fmt.Printf("Sending message: %v\n", m)
	return websocket.JSON.Send(ws, m)
}

//receiveMessage receives a Slack message through the given Websocket connection
func receiveMessage(ws *websocket.Conn) (m Message, err error) {
	//BUG: inviting chucky blows up here: Message receive error: json: cannot unmarshal object into Go value of type string
	err = websocket.JSON.Receive(ws, &m)
	return
}

//typeMessage simulates user typing
func typeMessage(ws *websocket.Conn, id string, m Message) {
	m.Type = "typing"
	m.User = id
	go sendMessage(ws, m)
	d, _ := time.ParseDuration(fmt.Sprintf("%ds", 1+rand.Intn(5)))
	time.Sleep(d)
}
