package main

import (
	"regexp"
	"strings"
)

var (
	ideaRegex    = regexp.MustCompile(`idea[:]? (?P<idea>.*)$`)
	ideaAddRegex = regexp.MustCompile(`add idea[:]? (?P<idea>.*)$`)
)

func containsAddIdeaCommand(text string) bool {
	//TODO: evolve to parse commands
	return ideaAddRegex.MatchString(text)
}

func parseIdea(text string) (s string) {
	matches := ideaRegex.FindStringSubmatch(text)
	if len(matches) > 0 {
		s = matches[len(matches)-1]
		if isQuestion(s) {
			s = trimSuffix(s, "?")
		}
	}
	return
}

func isQuestion(s string) (ok bool) {
	if last := len(s) - 1; last >= 0 && s[last] == '?' {
		ok = true
	}
	return
}

func trimSuffix(s, suffix string) string {
	if strings.HasSuffix(s, suffix) {
		s = s[:len(s)-len(suffix)]
	}
	return s
}
