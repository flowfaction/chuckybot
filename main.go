package main

import (
	"bitbucket.org/flowfaction/chuckybot/trello"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"
)

var chuckyOkResponses = []string{
	"*I accept that.*",
	"J'ai Obtenu Cette",
	"I am in motion, without commotion",
	"Indeed, indeed.  I do the deed.  He said, she said, with buoyant speed",
}

var chuckyErrResponses = []string{
	"I'm sorry man, sorry. I have this condition.",
	"I'm making chili for the fundraiser.",
}

const IDEA_LIST_ID = "57707db899fb818375b03bd5"

func main() {
	rand.Seed(time.Now().UnixNano())

	if len(os.Args) != 2 {
		handleCmdError()
	}

	token := os.Args[1]
	if token == "" || token == " " {
		handleCmdError()
	}

	client := trello.NewClient(trello.API_KEY, trello.TOKEN)

	chuckChanId := "D0B106YQL"

	ws, id := connectSlack(token)
	log.Printf("I accept that. - %s\n", id)

	//TODO: set up monitor chan

	for {
		m, err := receiveMessage(ws)
		// TODO: check error types?
		if err != nil {
			log.Printf("[ERROR] Message receive error: %s", err)
		}

		log.Printf("Message: ID: %d, Type: %s, Channel: %s, Text: %s\n", m.Id, m.Type, m.Channel, m.Text)

		// see if chucky is mentioned, or if we're on his channel
		if m.Type == "message" && (strings.HasPrefix(m.Text, "<@"+id+">") || m.Channel == chuckChanId) {
			typeMessage(ws, id, m)

			//TODO: add slash command support?

			if containsAddIdeaCommand(m.Text) {
				idea := parseIdea(m.Text)
				log.Printf("IDEA: %s\n", idea)
				//client.GetList(IDEA_LIST_ID)
				c := trello.NewCard(idea, idea, "", 1)
				if _, err := client.AddCard(IDEA_LIST_ID, c); err == nil {
					m.Text = chuckyOkResponses[rand.Intn(len(chuckyOkResponses)-1)]
				} else {
					m.Text = chuckyErrResponses[rand.Intn(len(chuckyErrResponses)-1)]
				}
			} else {
				allReponses := append(chuckyOkResponses, chuckyErrResponses...)
				m.Text = allReponses[rand.Intn(len(allReponses)-1)]
			}

			m.Type = "message"
			go sendMessage(ws, m)
			if err != nil {
				log.Printf("[ERROR] Message send error: %s", err)
			}
		}
	}
}

//handleCmdError handles command line error messaging and exiting
func handleCmdError() {
	fmt.Fprintf(os.Stderr, "usage: chuckybot {slack-bot-token}\n")
	os.Exit(1)
}
