package main

import "testing"

func TestParseIdea(t *testing.T) {
	testVals := []struct {
		s        string // input
		expected string // expected result
	}{
		{"chucky.. add idea foobar", "foobar"},
		{"please add idea foobar", "foobar"},
		{"@chucky add idea: blah", "blah"},
		{"@chucky kill yourself", ""},
	}

	for _, v := range testVals {
		actual := parseIdea(v.s)
		if actual != v.expected {
			t.Errorf("parseForIdea(%s): expected '%v', actual '%v'", v.s, v.expected, actual)
		}
	}
}

func TestContainsIdea(t *testing.T) {
	testVals := []struct {
		s        string // input
		expected bool   // expected result
	}{
		{"chucky.. add idea foobar", true},
		{"please add idea foobar", true},
		{"@chucky add idea: blah", true},
		{"@chucky kill yourself", false},
	}

	for _, v := range testVals {
		actual := containsAddIdeaCommand(v.s)
		if actual != v.expected {
			t.Errorf("containsIdeaCommand(%s): expected '%v', actual '%v'", v.s, v.expected, actual)
		}
	}
}

func TestIsQuestion(t *testing.T) {
	testVals := []struct {
		s        string // input
		expected bool   // expected result
	}{
		{"chucky.. add idea foobar?", true},
		{"please add idea foobar", false},
		{"@chucky add idea: blah", false},
		{"@chucky can you add idea kill yourself?", true},
	}

	for _, v := range testVals {
		actual := isQuestion(v.s)
		if actual != v.expected {
			t.Errorf("isQuestion(%s): expected '%v', actual '%v'", v.s, v.expected, actual)
		}
	}
}

func TestTrimSuffix(t *testing.T) {
	testVals := []struct {
		s        string // input
		expected string // expected result
	}{
		{"chucky.. add idea foobar", "chucky.. add idea foobar"},
		{"can you please add idea foobar?", "can you please add idea foobar"},
		{"@chucky add idea: blah", "@chucky add idea: blah"},
		{"@chucky kill yourself?", "@chucky kill yourself"},
	}

	for _, v := range testVals {
		actual := trimSuffix(v.s, "?")
		if actual != v.expected {
			t.Errorf("parseForIdea(%s): expected '%v', actual '%v'", v.s, v.expected, actual)
		}
	}
}
